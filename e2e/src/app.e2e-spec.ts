import { AppPage } from './app.po';
import { environment } from '../../src/environments/environment';
import { browser } from 'protractor';

const API_URL = environment.apiUrl;
const API_KEY = environment.apiKey;

const partialUrl = 'top-headlines?country=us&category=business';
const address = `${API_URL}/${partialUrl}&apikey=${API_KEY}`;

describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should contain news', async () => {
    const title: string = await getFirstNewsTitle(page);
    page.navigateTo();
    page.waitPageToLoad('home');
    expect(page.getParagraphText()).toContain(title);
  });

  it ('should show details', async()  => {
    const title: string = await getFirstNewsTitle(page);
    page.navigateTo()
    page.waitPageToLoad('home');
    page.clickElement('ion-card:first-child').then(() => {
      page.waitPageToLoad('details');
      page.getCurrentUrl().then (url => {
        expect(url).toContain('details');
        expect(page.getTextFromElement('ion-card-title')).toContain(title);
      });
    });
  });
});

async function getFirstNewsTitle(page: AppPage) {
  let title: string;
  const data = await page.getJson(address);
  const firstArticle = data['articles'][0];
  title = firstArticle['title'];
  return title;
}
