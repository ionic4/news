import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  waitPageToLoad(page: string) {
    browser.wait(protractor.ExpectedConditions.urlContains(page), 5000);
  }

  getCurrentUrl() {
    return new Promise((resolve) => {
      browser.getCurrentUrl().then(data => {
        resolve(data);
      });
    });
  }

  getTextFromElement(selector: string) {
    return element(by.css(selector)).getText();
  }

  getParagraphText() {
    return element(by.deepCss('app-root ion-content')).getText();
  }

  clickElement(selector: string) {
    return new Promise((resolve) => {
      element(by.css(selector)).click().then(() => {
        resolve();
      });
    });
  }

  getJson(address) {
    const https = require('https');
    return new Promise ((resolve, reject) => {
      https.get(address, function(response) {
        let bodyString = '';
        response.setEncoding('utf8');
        response.on('data', function(chunk) {
            bodyString += chunk;
        });
        response.on('end', function() {
            resolve(JSON.parse(bodyString));
        });
      });
    });
  }
}
