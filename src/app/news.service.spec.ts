import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { NewsService } from './news.service';

describe('NewsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
    });
  });

  it('should be created', () => {
    const service: NewsService = TestBed.get(NewsService);
    expect(service).toBeTruthy();
  });

  it ('should get data', async(() => {
    const service: NewsService = TestBed.get(NewsService);
    service
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      const result = data['status'];
      const items: any = data['articles'];
      expect(result).toEqual('ok');
      expect(Array.isArray(items)).toBeTruthy();
      expect(items.length).toBeGreaterThan(0);
    });
  }));

  it ('should contain author, publishedAt, urlToImage',async(() => {
    const service: NewsService = TestBed.get(NewsService);
    service
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      const items: any = data['articles'];
      const firstArticle = items[0];

      expect(typeof(firstArticle)).toEqual('object');
      expect(Object.keys(firstArticle)).toContain('author');
      expect(Object.keys(firstArticle)).toContain('publishedAt');
      expect(Object.keys(firstArticle)).toContain('urlToImage');
    });
  }));

  it ('should contain currentNews', () => {
    const service: NewsService = TestBed.get(NewsService);
    const article: any = {
      'author': 'Test', 'publishedAt': '2019-03-25T13:10:49', 'urlToImage': 'test.jpg'
    };
    service.currentNews = article;
    expect(typeof(service.currentNews)).toEqual('object');
    expect(Object.keys(service.currentNews)).toContain('author');
    expect(Object.keys(service.currentNews)).toContain('publishedAt');
    expect(Object.keys(service.currentNews)).toContain('urlToImage');

    expect(service.currentNews.author).toContain('Test');
    expect(service.currentNews.publishedAt).toContain('2019-03-25T13:10:49');
    expect(service.currentNews.urlToImage).toContain('test.jpg');
  });
});
