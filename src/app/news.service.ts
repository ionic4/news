import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  currentNews: any; // Member variable to hold data about selected (current) news item.

  constructor(private http: HttpClient) {} // This class is using HTTP connection, so it needs to be injected.

  /**
   * Method retrieves news from NewsApi API.
   * 
   * @param url string Part of url, that will define what news (coutry and category) will be retrieved.
   * @returns Observable<any> News articles returned based on query.
   */
  getData(url: string): Observable<any> {
    const address =`${environment.apiUrl}/${url}&apikey=${environment.apiKey}`;
    return this.http.get(address);
  }
}
