import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { DetailsPage } from './details.page';
import { NewsService } from '../news.service';
import { By } from '@angular/platform-browser';

describe('DetailsPage', () => {
  let component: DetailsPage;
  let fixture: ComponentFixture<DetailsPage>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        HttpClientModule
      ],
    })
    .compileComponents();
  }));

  beforeEach(inject([NewsService], (newsService) => {
    const article: any = {
      'author': 'Test', 'publishedAt': '2019-03-25T13:10:49', 'urlToImage': 'test.jpg',
      'title': 'Test Title', 'content': 'Test content'
    };
    newsService.currentNews = article;
    fixture = TestBed.createComponent(DetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should contain title on header', () => {
    de = fixture.debugElement.query(By.css('ion-title'));
    el = de.nativeElement;
    expect(el.textContent).toContain('Test');
  });

  it ('should contain publishedAt on header', () => {
    de = fixture.debugElement.query(By.css('ion-title'));
    el = de.nativeElement;

    expect(el.textContent).toContain('2019-03-25T13:10:49');
  });

  it ('should contain img src', () => {
    de = fixture.debugElement.query(By.css('ion-img'));
    el = de.nativeElement;
    expect(de.properties.src).toEqual('test.jpg');
  });

  it ('should contain card title', () => {
    de = fixture.debugElement.query(By.css('ion-card-title'));
    el = de.nativeElement;
    expect(el.textContent).toContain('Test Title');
  });

  it ('should contain content', () => {
    de = fixture.debugElement.query(By.css('ion-card-content p'));
    el = de.nativeElement;
    expect(el.textContent).toContain('Test content');
  });
});
