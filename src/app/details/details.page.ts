import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  heading: any; // Member variable for displaying heading (consists of multiple fields of news article).

  // NewsService is public, so it can be referenced from html template.
  constructor(public newsService: NewsService) { }

  ngOnInit() {
    // CurrentNews is containing that news article that was selected on Home page.
    this.heading = this.newsService.currentNews.author  + ' on ' + this.newsService.currentNews.publishedAt;
  }

}
