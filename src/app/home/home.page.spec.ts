import { CUSTOM_ELEMENTS_SCHEMA, DebugElement, NgModule } from '@angular/core';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HomePage } from './home.page';
import { By } from '@angular/platform-browser';
import { DetailsPage } from '../details/details.page';
import { NewsService } from '../news.service';
//import { Location } from '@angular/common';

import { Router } from '@angular/router';


describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let de: DebugElement;
  let el: HTMLElement;
  let location: Location;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [ HomePage, DetailsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        HttpClientModule,
        RouterTestingModule.withRoutes(
          [
            { path: 'details', component: DetailsPage}
          ]
        ),
      ],
      providers: [
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
    component = null;
    de = null;
    el = null;
    location = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it ('should display news with title and description', () => {
    const items: any =
    {
      'articles': [
        {'title': 'Test 1', 'description': 'Test description 1', 'urlToImage': 'test1.jpg'},
        {'title': 'Test 2', 'description': 'Test description 2', 'urlToImage': 'test2.jpg'},
        {'title': 'Test 3', 'description': 'Test description 3', 'urlToImage': 'test3.jpg'},
      ]
    };

    const firstNews = items.articles[0];
    component.items = items;
    fixture.detectChanges();
    de = fixture.debugElement.query(By.css('ion-card-content'));
    el = de.nativeElement;

    expect(el.textContent).toContain(firstNews.title);
    expect(el.textContent).toContain(firstNews.description);
  });

  it ('should open details', inject([NewsService, Router], (newService, router) => {
    const article: any = {
      'author': 'Test', 'publishedAt': '2019-03-25T13:10:49', 'urlToImage': 'test.jpg',
      'title': 'Test Title', 'content': 'Test content'
    };
    newService.currentNews = article;
    
    //console.log(activatedRoute);
    //console.log(component['router']);
    component.openNews(article);
    //console.log(component['router']);
    //console.log(component['router'].url);
    //console.log(activatedRoute);
    const urlChanges = router.location['urlChanges'];
    console.log(urlChanges);
    //location = TestBed.get(Location);
    //console.log(location);
    //console.log(location.path());
    

  }));
});
