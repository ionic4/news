import { Component, OnInit } from '@angular/core';
import { NewsService } from '../news.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  items: any; // Member variable that is used to display news articles.

  // Page uses newsService and routing.
  constructor(
    private newsService: NewsService,
    private router: Router) {
  }

  // Executed when page is initialized and displayed.
  ngOnInit() {
    // Subscribe to newService to get and display news.
    this.newsService
    .getData('top-headlines?country=us&category=business')
    .subscribe(data => {
      this.items = data;
    });
  }

  // Method for opening a single news article.
  openNews(article) {
    // Assing currentNews to service, that is shared by Home and Details pages. Assign currentNews
    // here and read it on Details page.
    this.newsService.currentNews = article;
    this.router.navigate(['/details']); // Navigate to details.
  }
}
